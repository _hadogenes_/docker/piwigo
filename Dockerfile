FROM php:7-apache

ARG PIWIGO_VERSION="latest"

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN DEBIAN_FRONTEND=noninteractive apt update -y \
  && DEBIAN_FRONTEND=noninteractive apt install -yy \
       wget \
       unzip \
       exiftool \
       ffmpeg\
       imagemagick \
       dcraw \
       mediainfo \
  && install-php-extensions gd pdo pdo_mysql mysqli imagick exif ldap \
  && DEBIAN_FRONTEND=noninteractive apt purge -y \
      	libc-dev \
  && DEBIAN_FRONTEND=noninteractive apt autoremove --purge \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN cd /var/www \
  && wget -q -O piwigo.zip http://piwigo.org/download/dlcounter.php?code=$PIWIGO_VERSION \
  && unzip piwigo.zip \
  && rm -rfv /var/www/html piwigo.zip \
  && mv piwigo /var/www/html

RUN mkdir /template \
  && mv galleries themes plugins local /template/ \
  && mkdir -p /var/www/html/_data/i /config \
  && chown -R www-data:www-data /var/www /template/ \
  && mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY piwigo_php.ini $PHP_INI_DIR/conf.d/
ADD entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

VOLUME ["/var/www/html/galleries", "/var/www/html/themes", "/var/www/html/plugins", "/var/www/html/local", "/var/www/html/_data/i", "/config"]

EXPOSE 80

#!/bin/bash

for d in $(ls /template); do
    if [ -z "$(ls -A /var/www/html/$d)" ]; then
        echo "Creating dir /var/www/html/$d/ from template"
        cp -a /template/$d/* /var/www/html/$d/
	chown -R www-data /var/www/html/$d/
    fi
done

if [ "${PIWIGO_MYSQL_ENGINE}" ]; then
    grep -Rn MyISAM /var/www/html/install | cut -d: -f1 | sort -u | while read file; do
        sed -i 's/MyISAM/'$PIWIGO_MYSQL_ENGINE'/' "$file"
    done
fi

source /etc/apache2/envvars
exec apache2ctl -D FOREGROUND


